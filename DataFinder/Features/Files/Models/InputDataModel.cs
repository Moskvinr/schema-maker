﻿namespace dataFinder.Features.Files.Models
{
    public class InputDataModel
    {   
        public string FrontendPath { get; set; }
        public string BackendAssemblyPath { get; set; }
        public string BackendPath { get; set; }
        public string FrontendBaseClass { get; set; }
        public string RegularExpression { get; set; }
        public string ResultPath { get; set; }
    }
}
