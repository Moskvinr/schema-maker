﻿using dataFinder.Features.Files.Models;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace dataFinder.Features.Files
{
    class FilesHandler
    {
        public InputDataModel GetInputData(string path)
        {
            try
            {
                InputDataModel dataModel;
                using (var sr = new StreamReader(path))
                {
                    dataModel = JsonConvert.DeserializeObject<InputDataModel>(sr.ReadToEnd());
                }

                return dataModel;
            }
            catch
            {
                throw new FileNotFoundException($"{path} file not found. It should be json file with \n'FrontendPath', " +
                    $"'BackendPath', 'BackendAssemblyPath', 'FrontendBaseClass', 'RegularExpression', 'ResultPath' parameters");
            }
        }

        public string GetFileContent(string path)
        {
            string str;
            using (var sr = new StreamReader(path))
            {
                str = sr.ReadToEnd();
            }
            return str;
        }

        public IEnumerable<string> GetAllFilesPath(string path, string extension)
        {
            var qExtension = extension.Contains('.') ? $"*{extension}" : $"*.{extension}";
            return Directory.EnumerateFiles(path, qExtension, SearchOption.AllDirectories).Where(x => !x.Contains("node_modules"));
        }

        public void OutputConvertedToTS(string name, string body, string outputDir)
        {
            if (!Directory.Exists(outputDir))
            {
                Directory.CreateDirectory(outputDir);
            }

            var fileName = Path.Combine(outputDir, $"{name}.ts");

            if (!File.Exists(fileName))
            {
                File.Create(fileName).Dispose();
            }
            using (var sw = new StreamWriter(fileName))
            {
                sw.Write(body);
            }
        }

        public void OutputTypeData(string path, List<object> outputValues)
        {
            using (var sw = new StreamWriter(path))
            {
                sw.Write(JsonConvert.SerializeObject(outputValues));
            }
        }


    }
}
