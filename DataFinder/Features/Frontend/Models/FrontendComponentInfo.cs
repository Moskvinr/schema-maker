﻿namespace dataFinder.Features.Frontend.Models
{
    class FrontendComponentInfo
    {
        public string ParentClass { get; set; }
        public string TypeName { get; set; }
        public string ComponentPath { get; set; }
    }
}
