﻿namespace dataFinder.Features.Frontend.Models
{
    public class FrontendHandlerData
    {
        public string Path { get; set; }
        public string BaseClass { get; set; }
    }
}
