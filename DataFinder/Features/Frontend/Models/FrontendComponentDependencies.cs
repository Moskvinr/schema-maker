﻿using System.Collections.Generic;

namespace dataFinder.Features.Frontend.Models
{
    class FrontendComponentDependencies
    {
        public LinkedList<string> ClassDependencies { get; set; }
        public bool ContainBaseClass { get; set; }
    }
}
