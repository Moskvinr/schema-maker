﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using dataFinder.Features.Files;
using dataFinder.Features.Frontend.Models;

namespace dataFinder.Features.Frontend
{
    class FrontendHandler
    {
        private readonly FilesHandler _filesHandler;
        private readonly FrontendHandlerData _dataForFrontend;
        private Dictionary<string, FrontendComponentInfo> _componentNameAndImp;
        private Dictionary<string, FrontendComponentDependencies> _componentDependencies;
        private string _baseComponentMethodName;

        public FrontendHandler(FilesHandler filesHandler, FrontendHandlerData dataForFrontend)
        {
            _filesHandler = filesHandler;
            _dataForFrontend = dataForFrontend;
            _componentNameAndImp = new Dictionary<string, FrontendComponentInfo>();
            _componentDependencies = new Dictionary<string, FrontendComponentDependencies>();
        }

        public IEnumerable<string> GetComponentsReturnableType()
        {
            FillComponentsData();
            BuildComponentDependencies();

            _componentDependencies = _componentDependencies
                .Where(x => x.Value.ContainBaseClass)
                .ToDictionary(x => x.Key, x => x.Value);

            FillChildComponentsReturnType();

            return _componentNameAndImp
                .Where(x => x.Key != _dataForFrontend.BaseClass && x.Value.TypeName != "")
                .Select(x => x.Value.TypeName);
        }

        private string GetTypeFromBody(string content, Regex regex, string startsWith, string endsWith)
        {
            int start, end;
            start = content.IndexOf(startsWith, 0) + startsWith.Length;
            end = content.IndexOf(endsWith, start);
            var methodBody = content.Substring(start, end - start);
            var type = regex
                .Matches(methodBody)
                .First()
                .Value
                .Split(' ')
                .Last()
                .Replace("'", "")
                .Replace(";", "");
            return type;
        }

        private void FillChildComponentsReturnType()
        {
            string startsWith = _baseComponentMethodName;
            string endsWith = "}";
            var regex = new Regex("return .*");
            foreach (var key in _componentDependencies.Keys)
            {
                var content = _filesHandler.GetFileContent(_componentNameAndImp[key].ComponentPath);
                _componentNameAndImp[key].TypeName = GetTypeFromBody(content, regex, startsWith, endsWith);
            }
        }

        private IEnumerable<string> GetTSFilesPath()
        {
            return _filesHandler.GetAllFilesPath(_dataForFrontend.Path, ".ts");
        }

        private void FillComponentsData()
        {
            var tsFilesPath = GetTSFilesPath();
            var regex = new Regex("class (.)* .*");
            string content;
            MatchCollection matches;
            string[] matchedLineWords;

            foreach (var filepath in tsFilesPath)
            {
                content = _filesHandler.GetFileContent(filepath);
                matches = regex.Matches(content);
                foreach (Match match in matches)
                {
                    matchedLineWords = match.Value.Split(' ');
                    if (matchedLineWords.Length > 3)
                    {
                        _componentNameAndImp.Add(matchedLineWords[1],
                            new FrontendComponentInfo
                            {
                                ParentClass = matchedLineWords[3],
                                TypeName = "",
                                ComponentPath = filepath
                            }
                          );
                    }
                    else if (matchedLineWords[1] == _dataForFrontend.BaseClass)
                    {
                        _componentNameAndImp.Add(matchedLineWords[1], null);
                        SetBaseComponentMethodName(content);
                    }
                }
            }
        }

        private void SetBaseComponentMethodName(string content)
        {
            var regex = new Regex(@"[a-zA-Z][a-zA-Z0-9]+\(");
            var match = regex.Match(content);
            _baseComponentMethodName = match.Value + ")";
        }

        private void BuildComponentDependencies()
        {
            foreach (var key in _componentNameAndImp.Keys)
            {
                string parentClass = null;
                if (_componentNameAndImp[key] != null)
                {
                    parentClass = _componentNameAndImp[key].ParentClass;
                }
                var parentsEnum = new LinkedList<string>();
                parentsEnum.AddFirst(key);
                var dep = new FrontendComponentDependencies();
                while (parentClass != null)
                {
                    parentsEnum.AddLast(parentClass);

                    if (parentClass.ToLower() == _dataForFrontend.BaseClass.ToLower())
                    {
                        dep.ContainBaseClass = true;
                    }

                    FrontendComponentInfo info = new FrontendComponentInfo();
                    _componentNameAndImp.TryGetValue(parentClass, out info);

                    if (info != null)
                    {
                        parentClass = info.ParentClass;
                    }

                    else
                    {
                        break;
                    }
                }
                dep.ClassDependencies = parentsEnum;
                _componentDependencies.Add(key, dep);
            }
        }
    }
}
