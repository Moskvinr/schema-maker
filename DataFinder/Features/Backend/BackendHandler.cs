﻿using dataFinder.Features.Backend.Models;
using dataFinder.Features.Files;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;

namespace dataFinder.Features.BackendHandler
{
    class BackendHandler
    {
        private readonly IEnumerable<string> _methodTypeNames;
        private readonly BackendHandlerData _backendData;
        private readonly FilesHandler _filesHandler;

        public BackendHandler(IEnumerable<string> methodTypeNames, BackendHandlerData backendData, FilesHandler filesHandler)
        {
            _methodTypeNames = methodTypeNames;
            _backendData = backendData;
            _filesHandler = filesHandler;
        }

        private IEnumerable<Type> GetClassTypes()
        {
            var assembly = Assembly.LoadFrom(_backendData.AssemblyPath);
            var backendClasses = assembly
                .GetTypes()
                .Where(x => x.IsClass);

            foreach (var clType in _methodTypeNames)
            {
                yield return backendClasses.FirstOrDefault(x => x.Name == clType);
            }
            //var regex = new Regex(_backendData.RegularExpression);
            //foreach(var backendClass in backendClasses)
            //{
            //    if (regex.IsMatch(backendClass.Name))
            //    {
            //        yield return backendClass;
            //    }
            //}
        }

        public List<object> GetObjectsWithTypesInfo()
        {
            List<object> objectValues = new List<object>();
            foreach (var classType in GetClassTypes())
            {
                objectValues.Add(new
                {
                    className = classType
                });
                //var constr = classType.GetConstructor(Type.EmptyTypes);
                //var cl = constr.Invoke(new object[] { });
                //foreach (var methodName in _methodTypeNames)
                //{
                //    var methodType = classType.GetMethod(methodName);
                //    if (methodType != null)
                //    {
                //        objectValues.Add(methodType.Invoke(cl, new object[] { }));
                //    }
                //}
            }
            return objectValues;
        }

        private string GetClassName(string body)
        {
            var regex = new Regex("class( )+([a-z]|[A-Z]|)+([0-9]|[a-z]|[A-Z])*");
            return regex.Match(body).Value.Split(' ').Last();
        }

        public List<BackendClassNameAndBody> GetClassesBody()
        {
            //var regex = new Regex("class( )*([a-z]|[A-Z]|)+([0-9]|[a-z]|[A-Z])*( )*(\r)*(\n)*{(\r)*(.)*(\n)*(\r)* }");
            List<BackendClassNameAndBody> classData = new List<BackendClassNameAndBody>();
            string className;
            foreach (var content in GetClassesContent())
            {
                className = GetClassName(content);
                if (_methodTypeNames.Any(x => x == className))
                {
                    classData.Add(new BackendClassNameAndBody
                    {
                        Body = content,
                        Name = className
                    });
                }
            }
            return classData;
        }

        private IEnumerable<string> GetClassesContent()
        {
            var csharpFilesPath = _filesHandler.GetAllFilesPath(_backendData.FilesPath, ".cs");
            foreach (var filePath in csharpFilesPath)
            {
                yield return _filesHandler.GetFileContent(filePath);
            }
        }
    }
}
