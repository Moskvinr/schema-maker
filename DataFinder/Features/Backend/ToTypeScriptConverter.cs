﻿using System;
using TypescriptSyntaxPaste;
using TypescriptSyntaxPaste.VSIX;

namespace dataFinder.Features.Backend
{
    class ToTypeScriptConverter
    {
        private readonly CSharpToTypescriptConverter _toTypescriptConverter;
        private readonly MySettingStore _settingStore;

        public ToTypeScriptConverter()
        {
            _toTypescriptConverter = new CSharpToTypescriptConverter();
            _settingStore = new MySettingStore();
        }

        public string ConvertToTypeScript(string content) =>
            _toTypescriptConverter.ConvertToTypescript(content, _settingStore);

    }

    class MySettingStore : ISettingStore
    {
        public bool AddIPrefixInterfaceDeclaration
        {
            get { return true; }

            set { throw new NotImplementedException(); }
        }

        public bool IsConvertListToArray
        {
            get { return true; }

            set { throw new NotImplementedException(); }
        }

        public bool IsConvertMemberToCamelCase
        {
            get { return true; }

            set { throw new NotImplementedException(); }
        }

        public bool IsConvertToInterface
        {
            get { return false; }

            set { throw new NotImplementedException(); }
        }

        public bool IsInterfaceOptionalProperties
        {
            get { return false; }

            set { throw new NotImplementedException(); }
        }

        public TypeNameReplacementData[] ReplacedTypeNameArray
        {
            get { return new TypeNameReplacementData[0]; }

            set { throw new NotImplementedException(); }
        }
    }

}
