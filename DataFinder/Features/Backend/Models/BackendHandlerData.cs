﻿namespace dataFinder.Features.Backend.Models
{
    class BackendHandlerData
    {
        public string AssemblyPath { get; set; }
        public string FilesPath { get; set; }
        public string RegularExpression { get; set; }
    }
}
