﻿using System;
using System.Collections.Generic;
using System.Text;

namespace dataFinder.Features.Backend.Models
{
    class BackendClassNameAndBody
    {
        public string Name { get; set; }
        public string Body { get; set; }
    }
}
