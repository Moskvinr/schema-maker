﻿using dataFinder.Features.Backend;
using dataFinder.Features.Backend.Models;
using dataFinder.Features.BackendHandler;
using dataFinder.Features.Files;
using dataFinder.Features.Files.Models;
using dataFinder.Features.Frontend;
using dataFinder.Features.Frontend.Models;

namespace dataFinder
{
    public class AppRunner
    {
        private readonly FilesHandler _filesHandler;
        private readonly InputDataModel _inputData;

        public AppRunner()
        {
            _filesHandler = new FilesHandler();
            _inputData = _filesHandler.GetInputData(@"Data\configFile.json");
        }

        public void AppRun()
        {
            var frontendHandler = new FrontendHandler(_filesHandler, GetDataForFrontendHandler());
            var componentTypeNames = frontendHandler.GetComponentsReturnableType();

            var backendHandler = new BackendHandler(componentTypeNames, GetDataForBackendHandler(), _filesHandler);
            var types = backendHandler.GetObjectsWithTypesInfo();
            _filesHandler.OutputTypeData(_inputData.ResultPath, types);

            OutputClassesToTS(backendHandler, @"TSFiles");
            
        }

        private void OutputClassesToTS(BackendHandler backendHandler, string outputDir)
        {
            var toTsConverter = new ToTypeScriptConverter();
            foreach(var classBody in backendHandler.GetClassesBody())
            {
                var tsBody = toTsConverter.ConvertToTypeScript(classBody.Body);
                _filesHandler.OutputConvertedToTS(classBody.Name, tsBody, outputDir);
            }
        }

        private FrontendHandlerData GetDataForFrontendHandler()
        {
            return new FrontendHandlerData()
            {
                BaseClass = _inputData.FrontendBaseClass,
                Path = _inputData.FrontendPath
            };
        }

        private BackendHandlerData GetDataForBackendHandler()
        {
            return new BackendHandlerData()
            {
                AssemblyPath = _inputData.BackendAssemblyPath,
                FilesPath = _inputData.BackendPath,
                RegularExpression = _inputData.RegularExpression
            };
        }

    }
}
