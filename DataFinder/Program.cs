﻿using System;
using TypescriptSyntaxPaste;
using TypescriptSyntaxPaste.VSIX;

namespace dataFinder
{
    class Program
    {
        static void Main(string[] args)
        {
            new AppRunner().AppRun();
        }
    }
}
