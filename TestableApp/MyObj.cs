﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestableApp
{
    class MyObjectModel
    {
        public string Name { get; set;}
        public string Value { get; set; }
    }

    class MyObj
    {

        public MyObj() { }

        public MyObjectModel First()
        {
            return new MyObjectModel
            {
                Name = "first",
                Value = "FirstM"
            };
        }
        public MyObjectModel Second()
        {
            return new MyObjectModel
            {
                Name = "second",
                Value = "SecondM"
            };
        }
        public MyObjectModel Third()
        {
            return new MyObjectModel
            {
                Name = "third",
                Value = "ThirdM"
            };
        }
        public string Fourth()
        {
            return "Fourth";
        }
    }
}
