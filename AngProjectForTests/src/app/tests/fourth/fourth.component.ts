import { Component, OnInit } from '@angular/core';
import { BaseClass } from '../base-class';

@Component({
  selector: 'app-fourth',
  templateUrl: './fourth.component.html',
  styleUrls: ['./fourth.component.css']
})
export class FourthComponent implements BaseClass {
  MyMethod(): string {
    return 'Fourth';
  }

  constructor() { }

}
