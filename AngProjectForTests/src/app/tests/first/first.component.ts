import { Component, OnInit } from '@angular/core';
import { BaseClass } from '../base-class';

@Component({
  selector: 'app-first',
  templateUrl: './first.component.html',
  styleUrls: ['./first.component.css']
})
export class FirstComponent implements BaseClass {
  MyMethod(): string {
    return 'First';
  }

  constructor() { }

}
