import { Component, OnInit } from '@angular/core';
import { FirstComponent } from '../first/first.component';

@Component({
  selector: 'app-second',
  templateUrl: './second.component.html',
  styleUrls: ['./second.component.css']
})
export class SecondComponent implements FirstComponent {
  MyMethod(): string {
    return 'Second';
  }

  constructor() { }


}
